from google.colab import files

import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix, hstack

from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc

import pylab as pl
import seaborn as sns
from matplotlib import pyplot as plt

file = files.upload()

train_df = pd.read_csv('train_sessions.csv', index_col='session_id')
test_df = pd.read_csv('test_sessions.csv', index_col='session_id')
train_df.head(10)

train_df['target'].value_counts()

train_test_df = pd.concat([train_df, test_df])

"""## Part 1. Only sites"""

sites = ['site%d' % i for i in range(1, 11)]
train_test_df_sites = train_test_df[sites].fillna(0).astype('int')
train_test_df_sites.head(5)

def csrmatrix(docs):
    indptr = [0]
    indices = []
    data = []
    for d in docs:
        for term in d:
            indices.append(term)
            data.append(1)
        indptr.append(len(indices))

    return csr_matrix((data, indices, indptr), dtype=int)[:,1:]

train_test_sparse = csrmatrix(train_test_df_sites.values)
X_train_sparse = train_test_sparse[:train_df.shape[0]]
X_test_sparse = train_test_sparse[train_df.shape[0]:]
y = train_df.iloc[:, -1].values

divider = int(.7 * X_train_sparse.shape[0])
X_train, y_train = X_train_sparse[:divider, :], y[:divider]
X_test, y_test  = X_train_sparse[divider:, :], y[divider:]

"""Stochastic Gradient Descent Classifier"""

parameter_grid = {
            'loss': ['log', 'perceptron'],
            'penalty': ['l1', 'l2'],
            'alpha': [1e-04, 1e-07, 1e-10]
          }
clf = SGDClassifier()
rand_searcher = RandomizedSearchCV(clf, parameter_grid, scoring='roc_auc', n_iter=3, random_state=0)
rand_searcher.fit(X_train, y_train)
print(rand_searcher.best_score_)
print(rand_searcher.best_params_)

sgd_clf = SGDClassifier(loss='log', alpha=1e-07, random_state=17, n_jobs=-1)
sgd_clf.fit(X_train, y_train)
sgd_test_pred_proba = sgd_clf.predict_proba(X_test)
roc_auc_score(y_test, sgd_test_pred_proba[:, 1])

"""Random Forest Classifier"""

parameter_grid = {
            'criterion': ['entropy', 'gini'],
            'max_depth': [80, 100, 120],
            'n_estimators': [80, 100, 120]
          }  
clf = RandomForestClassifier()
rand_searcher = RandomizedSearchCV(clf, parameter_grid, scoring='roc_auc', n_iter=3, random_state=0)
rand_searcher.fit(X_train, y_train)
print(rand_searcher.best_score_)
print(rand_searcher.best_params_)

rf_clf = RandomForestClassifier(criterion='entropy', max_depth=120, n_estimators=120)
rf_clf.fit(X_train, y_train)
rf_test_pred_proba = rf_clf.predict_proba(X_test)
roc_auc_score(y_test, rf_test_pred_proba[:, 1])

"""Logistic Regression"""

lr_clf = LogisticRegression(random_state=1, max_iter = 300)
lr_clf.fit(X_train, y_train)
lr_test_pred_proba = lr_clf.predict_proba(X_test)
roc_auc_score(y_test, lr_test_pred_proba[:, 1])

"""Graph ROC AUC"""

pl.clf()
plt.figure(figsize=(8,6))

proba = sgd_clf.fit(X_train, y_train).predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, proba[:, 1])
roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('SGDClasifier', roc_auc))
proba = rf_clf.fit(X_train, y_train).predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, proba[:, 1])
roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('RandomForest', roc_auc))
proba = lr_clf.fit(X_train, y_train).predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, proba[:, 1])
roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('LogisticRegression', roc_auc))

pl.plot([0, 1], [0, 1], 'k--')
pl.xlim([0.0, 1.0])
pl.ylim([0.0, 1.0])
pl.xlabel('False Positive Rate')
pl.ylabel('True Positive Rate')
pl.legend(loc=0, fontsize='small')
pl.show()





"""## Part 2. Sites + new features"""

sites = ['site%d' % i for i in range(1, 11)]
times = ['time%d' % i for i in range(1, 11)]
train_test_df_time = train_test_df[times].fillna(0)
new_feat = pd.DataFrame(index=train_test_df_time.index)
new_feat['start_hour'] = pd.DatetimeIndex(train_test_df_time['time1']).hour
new_feat['start_month'] = pd.DatetimeIndex(train_test_df_time['time1']).month
new_feat['day_of_week'] = pd.DatetimeIndex(train_test_df_time['time1']).dayofweek
new_feat['unique_sites'] = train_test_df[sites].nunique(axis=1,  dropna=True)
new_feat.head()

new_feat_dum = pd.get_dummies(new_feat.loc[:, ['start_hour', 'start_month', 'day_of_week', 'unique_sites']],
                                  columns=new_feat.loc[:, ['start_hour', 'start_month', 'day_of_week', 'unique_sites']].columns,
                                  drop_first=False)

new_feat_sparse = csr_matrix(new_feat_dum)

all_sparse = csr_matrix(hstack([train_test_sparse, new_feat_sparse]))
X_train_sparse = all_sparse[:train_df.shape[0]]
X_test_sparse = all_sparse[train_df.shape[0]:]
y = train_df.iloc[:, -1].values

divider = int(.7 * X_train_sparse.shape[0])
X_train, y_train = X_train_sparse[:divider, :], y[:divider]
X_test, y_test  = X_train_sparse[divider:, :], y[divider:]



"""Stochastic Gradient Descent Classifier"""

parameter_grid = {
            'loss': ['log', 'perceptron'],
            'penalty': ['l1', 'l2'],
            'alpha': [1e-04, 1e-05, 1e-06, 1e-07]
          }
clf = SGDClassifier()
rand_searcher = RandomizedSearchCV(clf, parameter_grid, scoring='roc_auc', n_iter=3, random_state=0)
rand_searcher.fit(X_train, y_train)
print(rand_searcher.best_score_)
print(rand_searcher.best_params_)

sgd_clf = SGDClassifier(loss='log', alpha=1e-05, random_state=17, n_jobs=-1)
sgd_clf.fit(X_train, y_train)
sgd_test_pred_proba = sgd_clf.predict_proba(X_test)
roc_auc_score(y_test, sgd_test_pred_proba[:, 1])

"""Random Forest Classifier"""

parameter_grid = {
            'criterion': ['entropy', 'gini'],
            'max_depth': [10, 30, 100],
            'n_estimators': [10, 30, 100]
          }  
clf = RandomForestClassifier()
rand_searcher = RandomizedSearchCV(clf, parameter_grid, scoring='roc_auc', n_iter=3, random_state=0)
rand_searcher.fit(X_train, y_train)
print(rand_searcher.best_score_)
print(rand_searcher.best_params_)

rf_clf = RandomForestClassifier(criterion='entropy', max_depth=100, n_estimators=100)
rf_clf.fit(X_train, y_train)
rf_test_pred_proba = rf_clf.predict_proba(X_test)
roc_auc_score(y_test, rf_test_pred_proba[:, 1])

"""Logistic Regression"""

lr_clf = LogisticRegression(random_state=1, max_iter = 300)
lr_clf.fit(X_train, y_train)
lr_test_pred_proba = lr_clf.predict_proba(X_test)
roc_auc_score(y_test, lr_test_pred_proba[:, 1])

"""Graph ROC AUC"""

pl.clf()
plt.figure(figsize=(8,6))

proba = sgd_clf.fit(X_train, y_train).predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, proba[:, 1])
roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('SGDClasifier', roc_auc))
proba = rf_clf.fit(X_train, y_train).predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, proba[:, 1])
roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('RandomForest', roc_auc))
proba = lr_clf.fit(X_train, y_train).predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, proba[:, 1])
roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, label='%s ROC (area = %0.2f)' % ('LogisticRegression', roc_auc))

pl.plot([0, 1], [0, 1], 'k--')
pl.xlim([0.0, 1.0])
pl.ylim([0.0, 1.0])
pl.xlabel('False Positive Rate')
pl.ylabel('True Positive Rate')
pl.legend(loc=0, fontsize='small')
pl.show()









lr_clf.fit(X_train_sparse, y)
sgd_test_pred_proba = lr_clf.predict_proba(X_test_sparse)
result = sgd_test_pred_proba[:,1]

def write_to_submission_file(predicted_labels, out_file,
                             target='target', index_label="session_id"):
    # turn predictions into data frame and save as csv file
    predicted_df = pd.DataFrame(predicted_labels,
                                index = np.arange(1, predicted_labels.shape[0] + 1),
                                columns=[target])
    predicted_df.to_csv(out_file, index_label=index_label)

write_to_submission_file(result, 'result.csv')

files.download('result.csv')

